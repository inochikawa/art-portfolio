const path = require('path');
var ExtractTextPlugin = require("extract-text-webpack-plugin");

module.exports = {
    entry: {
        scripts: ["./src/index.tsx"],
        styles: ["./src/styles/root.less"]
    },
    cache: false,
    output: {
        filename: "[name].bundle.js",
        path: path.resolve(__dirname, 'dist'),
        publicPath: ""
    },
    resolve: {
        extensions: ['.js', '.json', '.ts', '.tsx', '.less'],
    },
    plugins: [
        new ExtractTextPlugin("[name].css"),
    ],
    devtool: 'source-map',
    devServer: {
        contentBase: path.join(__dirname, "dist"),
        compress: true,
        port: 3300
    },
    module: {
        rules: [
            {
                test: /\.(ts|tsx)$/,
                loader: "awesome-typescript-loader"
            },
            {
                test: /\.css$/,
                use: ExtractTextPlugin.extract({
                    use: ['style-loader', 'css-loader'],
                    publicPath: ''
                }),
                include: [
                    path.resolve(__dirname, "./src/styles")
                ]
            },
            {
                test: /\.less$/,
                use: ExtractTextPlugin.extract({
                    fallback: 'style-loader',
                    use: ['css-loader', 'less-loader'],
                    publicPath: ''
                })
            }
        ]
    },
};